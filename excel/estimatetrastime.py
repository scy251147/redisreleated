#! /usr/bin/env python
#coding=utf-8
from __future__ import unicode_literals

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import xlrd
import time
from sqlalchemy import create_engine

__author__ = 'shichaoyang'

db_test = 'zxadmin:2ee8c137b56f2ec@10.130.91.54:3550'

time_fmt='%Y-%m-%d %X'

#INSERT INTO delivery_company(delivery_company_name,TIMESTAMP) VALUES('FEDEX',NOW());
#INSERT INTO delivery_company(delivery_company_name,TIMESTAMP) VALUES('DHL',NOW());
#INSERT INTO delivery_company(delivery_company_name,TIMESTAMP) VALUES('AIT/CEVA',NOW());

def put_data_to_file(filename, filecontent):
    keyfile= file(filename, "w+")
    keyfile.writelines(filecontent)
    keyfile.close()

def data_import_portal(work_sheet_excel , message_print):
    delivery_time_sql = ""
    for i in range(1, work_sheet_excel.nrows):
        shipping_method =str(work_sheet_excel.cell(i, 0).value).capitalize().lower()

        zip_code_raw = str(long(work_sheet_excel.cell(i, 1).value))
        zip_code = unicode(zip_code_raw, "utf-8").rjust(5, "0")

        #print zip_code

        east = str(int(work_sheet_excel.cell(i, 2).value))
        west = str(int(work_sheet_excel.cell(i, 3).value))
        p_min = str(int(work_sheet_excel.cell(i, 4).value))
        p_max = str(int(work_sheet_excel.cell(i, 5).value))

        #print zip_code+"|"+east+"|"+west+"|"+p_min+"|"+p_max

        company_id = company_dict[shipping_method]
        insert_sql = "insert into delivery_time(delivery_company_id,zip_code,delivery_time_east,delivery_time_west,presale_produce_time_max,presale_produce_time_min,timestamp) " \
                     "values("+company_id+",'"+zip_code+"',"+east+","+west+","+p_max+","+p_min+",'"+time.strftime(time_fmt, time.localtime())+"');\n"
        #print insert_sql

        delivery_time_sql = delivery_time_sql+ insert_sql

    #print delivery_time_sql
    put_data_to_file(message_print, delivery_time_sql)
    connection.execute(delivery_time_sql)

    print message_print

if __name__ == "__main__":
    work_book = xlrd.open_workbook('Estimated_Delivery_Time_Final.xlsx')
    print work_book.sheet_names()
    engine_address_shipping = create_engine('mysql://%s/address_shipping?charset=utf8&use_unicode=1' % db_test)
    connection = engine_address_shipping.connect()

    ###### init carrier ######
    connection.execute("delete from delivery_product")
    connection.execute("delete from delivery_time")

    company_dict = {}
    delivery_company_query = connection.execute("select * from delivery_company")

    for row in delivery_company_query:
        company_id = str(row["delivery_company_id"])
        company_name = str(row["delivery_company_name"]).lower()
        company_dict[company_name] = company_id

    print company_dict

    ###### import delivery_company data  ######
    work_sheet = work_book.sheet_by_name('Product-Carrier')
    delivery_product_sql = ""

    for i in range(1, work_sheet.nrows):
        product_id =str(long(work_sheet.cell(i, 0).value))
        shipping_method = str(work_sheet.cell(i, 1).value).capitalize().lower()
        company_id = company_dict[shipping_method]
        insert_sql = "insert into delivery_product(delivery_company_id,product_id,timestamp) " \
                     "values("+company_id+",'"+product_id+"','"+time.strftime(time_fmt, time.localtime())+"');\n"
        #print insert_sql

        delivery_product_sql = delivery_product_sql+ insert_sql

    #print delivery_product_sql
    put_data_to_file("Product-Carrier", delivery_product_sql)
    connection.execute(delivery_product_sql)
    print "delivery product data initialize ok!"

    ###### impot delivery_time data for ait&ceva ######
    work_sheet_aitceva = work_book.sheet_by_name("AIT&CEVA")
    data_import_portal(work_sheet_aitceva, "delivery time for AIT&CEVA data initialize ok!")

    ###### impot delivery_time data for fedex ######
    work_sheet_fedex = work_book.sheet_by_name("FEDEX")
    data_import_portal(work_sheet_fedex, "delivery time for FEDEX data initialize ok!")

    ###### impot delivery_time data for dhl ######
    work_sheet_dhl = work_book.sheet_by_name("DHL")
    data_import_portal(work_sheet_dhl, "delivery time for DHL data initialize ok!")

    ###### data filter ######
    filter_sql = "delete from delivery_time where zip_code in ('00544','01133','01195','01580','01582','01806','01807','01808','02031','02207','02216','02239','02295','02298','02636','02854','04075','04467','04851','05875','06386','06454','06497','06832','06842','07182','07194','07309','07477','07983','08640','08641','08905','08922','08988','10015','10046','10047','10048','10072','10079','10082','10094','10096','10098','10099','10149','10184','10196','10197','10200','10557','10558','10571','10572','10943','11025','11041','11043','11044','11099','11240','11244','11248','11254','11255','11390','11535','11536','11592','11594','11595','11597','11708','11736','11750','11760','11774','11855','12593','13837','14645','14664','14673','14683','15263','15266','15273','15285','15288','15740','16215','16532','16533','16554','16918','17008','17091','17270','17326','17335','17738','17773','17942','18175','18514','18522','19388','19483','19485','19487','19488','19489','19640','19726','19887','19889','20107','20193','20199','21098','21260','21261','21265','21268','21274','21283','21606','21681','21682','21683','21684','21685','21686','21687','21688','21748','22047','22092','22093','22120','22184','22218','22222','22223','22229','22234','22321','22336','22721','23101','23240','23512','23825','24044','24045','24048','24205','24512','24544','24842','24961','25303','25309','25429','25926','25965','26461','27031','27151','27156','27220','27321','27322','27395','27480','27564','27854','27930','28674','29318','29390','29391','29573','29698','29915','30073','30092','30330','30347','30376','30379','30386','30387','30389','30390','30399','30544','30596','30911','30913','31120','31144','31169','31191','31197','31198','31199','31212','32149','32215','32230','32267','32290','32454','32590','32592','32613','32782','32799','32890','32893','32898','33107','33110','33121','33148','33195','33206','33439','33447','33651','33690','33900','35041','35225','35230','35240','35245','35263','35277','35278','35279','35280','35281','35286','35289','35299','36210','36449','36462','36501','36621','36622','36690','36762','37237','37245','37247','37248','37249','37990','38110','38142','38165','38227','38609','38628','38675','38749','38945','39072','39081','39235','40386','40495','40754','40931','40999','41307','41313','41333','41338','41362','41433','41747','42084','42283','42287','42364','42375','42403','43098','43163','43196','43198','43265','43299','43307','43618','43789','44178','44185','44189','44322','44393','44399','44631','44999','45025','45026','45043','45110','45138','45145','45228','45400','45408','45418','45427','45454','45463','46085','46223','46604','46620','47139','47430','47490','47614','47727','47739','47741','47744','47811','47812','47856','47864','47934','47935','47936','47937','47938','47939','48559','48736','48769','48802','48863','48921','48950','49069','49121','49550','49790','50099','50347','50397','50706','50982','50983','51344','51602','52319','52350','52538','53199','53235','53244','55161','55169','55177','55191','55468','55555','55561','55563','55579','56901','56902','56904','56915','56920','56933','56944','56945','56950','56965','56972','57056','57079','57188','57189','57192','57194','57195','57196','57253','58319','59344','59748','59773','59853','60049','60092','60125','60170','60290','60570','60597','60663','60679','61311','62224','62713','62746','62805','62857','63001','63190','63196','63198','63702','63963','64002','64172','64183','64185','64191','64192','64193','64194','64447','64789','64869','64944','65645','65701','65776','65817','66054','66077','66160','66250','66276','66279','66628','66637','66642','66652','66653','66692','68181','70097','70140','70149','70500','70595','70883','70891','71208','71217','71768','71844','71951','72189','72198','72439','73039','73094','73193','73197','73198','73199','73451','73454','74183','74184','74189','74194','74542','75037','75245','75258','75286','75310','75323','75334','75340','75343','75344','75353','75363','75364','75386','75387','75388','75396','76299','76545','76546','76795','77097','77246','77247','77250','77260','77276','77278','77285','77286','77294','77296','77298','77869','78234','78262','78275','78286','78287','78461','78470','78471','78473','78474','78475','78476','78477','78478','78780','78781','78785','78786','78788','78789','78798','79187','79320','79405','80028','80279','80280','80321','80322','80323','80328','80329','80840','80841','80940','81034','81075','81127','81134','81153','83721','83727','83730','83733','83757','84119','84120','84128','84144','84717','85055','85077','85096','85097','85099','85217','85218','85219','85220','85221','85222','85223','85227','85228','85230','85231','85232','85235','85237','85238','85239','85240','85241','85242','85243','85245','85247','85272','85273','85278','85279','85289','85290','85291','85292','85293','85294','85313','85671','85777','86330','86435','86555','87165','87365','88542','89824','90102','90103','90313','90397','90398','90612','90659','90671','90842','90845','90888','91131','91191','91363','91388','91399','91497','91714','91795','91797','91798','91799','91841','91990','92090','92133','92162','92184','92194','92292','92414','92424','92709','92710','92725','93093','93381','93382','93780','93784','94013','94101','94106','94135','94136','94138','94150','94152','94153','94154','94155','94156','94162','94171','94175','94199','94208','94245','94278','94625','95250','95314','95799','96031','97251','97253','97254','97255','97259','97269','97271','97272','97299','97313','97372','97425','97427','97428','97472','97482','97710','98054','98151','98171','98184','98189','98283','98442','98450','98455','98460','98477','98492','98667','98929','99165','99299');"
    put_data_to_file("filter-sql", filter_sql)
    connection.execute(filter_sql)

    print "data filter ok!"

