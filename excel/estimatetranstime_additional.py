#! /usr/bin/env python
#coding=utf-8
from __future__ import unicode_literals

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import xlrd
import time
from sqlalchemy import create_engine

__author__ = 'shichaoyang'

db_test = 'zxadmin:2ee8c137b56f2ec@10.130.91.54:3550'

time_fmt='%Y-%m-%d %X'

def put_data_to_file(filename, filecontent):
    keyfile= file(filename, "w+")
    keyfile.writelines(filecontent)
    keyfile.close()

if __name__ == "__main__":
    work_book = xlrd.open_workbook('Additional product data.xlsx')
    print work_book.sheet_names()
    engine_address_shipping = create_engine('mysql://%s/address_shipping?charset=utf8&use_unicode=1' % db_test)
    connection = engine_address_shipping.connect()

    company_dict = {}
    delivery_company_query = connection.execute("select * from delivery_company")

    for row in delivery_company_query:
        company_id = str(row["delivery_company_id"])
        company_name = str(row["delivery_company_name"]).lower()
        company_dict[company_name] = company_id

    print company_dict

    ###### import delivery_company data  ######
    work_sheet = work_book.sheet_by_name('Additional Product Data')
    delivery_product_sql = ""

    for i in range(1, work_sheet.nrows):
        product_id =str(long(work_sheet.cell(i, 0).value))
        shipping_method = str(work_sheet.cell(i, 1).value).capitalize().lower()
        company_id = company_dict[shipping_method]
        insert_sql = "insert into delivery_product(delivery_company_id,product_id,timestamp) " \
                     "values("+company_id+",'"+product_id+"','"+time.strftime(time_fmt, time.localtime())+"');\n"
        #print insert_sql

        delivery_product_sql = delivery_product_sql+ insert_sql

    #print delivery_product_sql
    put_data_to_file("Additional Product Data", delivery_product_sql)
    connection.execute(delivery_product_sql)
    print "delivery product data initialize ok!"


