#! /usr/bin/env python
#coding=utf-8
from __future__ import unicode_literals

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import xlrd
from sqlalchemy import create_engine

__author__ = 'shichaoyang'
# ref chapter: http://nullege.com/codes/search/sqlalchemy.engine.result.ResultProxy

db_test = 'zxrutestadmin:ru_test80d2c0aecf3@10.110.95.38:3550'
db_live = 'zxruadmin_rw:MzNjZWZhZmFiOWQ2NDBi@ru.wdb2.n.lemall.com:3550'

if __name__ == "__main__":
    work_book = xlrd.open_workbook('excel_repo/CityDelivery11-08.xlsx')
    work_sheet = work_book.sheet_by_name('Лист1')

    #pls add ?charset=utf8&use_unicode=1 or u can't get right encode
    engine_address_shipping = create_engine('mysql://%s/address_shipping?charset=utf8&use_unicode=1' % db_test)

    connection = engine_address_shipping.connect()
    transactin = connection.begin()
    print "transaction start"
    try:

        total_sql = []

        for i in range(1, work_sheet.nrows):

            state_name = work_sheet.cell(i, 0).value.capitalize()
            city_name = work_sheet.cell(i, 1).value.capitalize()
            is_cod = work_sheet.cell(i, 2).value.capitalize()

            select_sql = "select area_id from area where level=2 and area_name='"+state_name+"'"
            #print sql
            area_query = connection.execute(select_sql)

            state_id = -1l
            for row in area_query:
                state_id = row["area_id"]

            #print "state:" + state_name + "(" + str(state_id) + ")" + " city:" + city_name + " is_cod:" + is_cod

            is_cod_fmt = 1
            if is_cod == "No":
                is_cod_fmt = 0

            update_sql = "update area set iscod= "+str(is_cod_fmt)+" where area_name='"+city_name+"' and parent_id="+str(state_id)+";"

            total_sql.append(update_sql)

            print update_sql

        print "sql execution start"

        sqlfile= file("update_area_iscod.txt", "w+")
        sqlfile.writelines(total_sql)
        sqlfile.close()

        connection.execute(''.join(total_sql))
        transactin.commit()
        print "transaction complete."
    except:
        transactin.rollback()
        print "rolling back, why?"
        raise

    finally:
        connection.close()
        print "database close."