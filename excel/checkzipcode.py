#! /usr/bin/env python
#coding=utf-8
from __future__ import unicode_literals

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import xlrd
import time
from sqlalchemy import create_engine

__author__ = 'shichaoyang'

db_test = 'zxadmin:2ee8c137b56f2ec@10.130.91.54:3550'

if __name__ == "__main__":
    work_book = xlrd.open_workbook('Estimated_Delivery_Time_Final.xlsx')
    print work_book.sheet_names()
    engine_address_shipping = create_engine('mysql://%s/address_shipping?charset=utf8&use_unicode=1' % db_test)
    connection = engine_address_shipping.connect()

    work_sheet = work_book.sheet_by_name('DHL')

    zip_code_all = []
    for i in range(1, work_sheet.nrows):
        zip_code_raw = str(long(work_sheet.cell(i, 1).value))
        zip_code = unicode(zip_code_raw, "utf-8").rjust(5, "0")
        zip_code_all.append(zip_code)

    select_sql = "SELECT * FROM zip_code;"
    select_result = connection.execute(select_sql)

    zip_code_all_database = {}
    zip_code_not_exist=[]
    for row in select_result:
        zip = str(row["zip"])
        zip_code_all_database[zip]= 99

    for i in zip_code_all:
        try:
            result = zip_code_all_database[i]
        except:
            zip_code_not_exist.append("'"+i+"'")

    print ','.join(zip_code_not_exist)
