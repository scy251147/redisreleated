#! /usr/bin/env python
# -*- coding: utf-8 -*-

from cluster_sync_executor import work_go

__author__ = 'shichaoyang'

def notify():
    result = work_go.apply_async(queue='laplace')
    return result

if __name__ == '__main__':
    sync_result = notify()
    print sync_result.status
    print sync_result.id
    print sync_result.get(timeout=1000)