#! /usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from celery import Celery
from celery.utils.log import get_task_logger
from datetime import timedelta

__author__ = 'shichaoyang'

logger = get_task_logger(__name__)

broker  = 'redis://:letvsc@2015@10.154.80.199:6379/1'
backend = 'redis://:letvsc@2015@10.154.80.199:6379/2'

app = Celery('tasks'
            , broker=broker
            , backend=backend
            , include=['cluster_sync_executor']
            )

app.conf.timezone = 'UTC'
app.conf.beat_schedule = {
    'add-every-30-seconds': {
        'task': 'cluster_sync_executor.work_go.delay()',
        'schedule': 30.0,
        'args': (16, 16)
    },
}
app.conf.update(
                CELERY_ACKS_LATE=True
              , CELERY_ACCEPT_CONTENT=['pickle','json']
              , CELERYD_FORCE_EXECV=True
              , CELERYD_MAX_TASKS_PER_CHILD=500
              , BROKER_HEARTBEAT=0
               )


if __name__ == '__main__':
    app.start()