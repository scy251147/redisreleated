__author__ = 'shichaoyang'

#--------apply usage--------
def function(a,b):
    print a,b

apply(function,("hello","python"))

#-------- apply to call  --------

class Rectangle:
    def __init__(self,color="red",width=10,height=10):
        print "create a",color,self,"sized",width,"x",height

class RoundedRectangle(Rectangle):
    def __init__(self,**kw):
        apply(Rectangle.__init__,(self,),kw)

rect = Rectangle(color="green",height=100,width=100)
rect = RoundedRectangle(color="blue",height=20)

#------------------

book="how to learn python."
pages=249
scripts=349

print "the %(book)s book contains more than %(scripts)s scripts" %vars()

#---------------------

strm = ""
strx = ""
stru = ""
for i in range(1, 11):
    strm = strm + "active_rush"+str(i)+" "
    strx = strx + "active_rush_key_set"+str(i)+" "
    stru = stru + "user_rush"+str(i)+" "
print strm
print strx
print stru

st = ""
for i in range(1,101):
    s = "active_rush"+str(i)
    for j in range(1,11):
        m = s+"-"+str(j)
        st=st+m+" "

print st


print