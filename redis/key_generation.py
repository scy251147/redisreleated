__author__ = 'shichaoyang'

import redis

if __name__ == "__main__":

    # hash: rush_info_map
    # hash: user_rush+rushId
    # zset: active_rush_key_set

    key_max = 201

    ##########=====>rush_info_map
    container = ["rush_info_map"]

    ##########=====>user_rush+rushId
    for i in range(1, key_max):
        container.append("user_rush"+str(i))

    ##########=====>active_rush_key_set+rushId
    ####active_rush_key_set(1~200)
    ####active_rush(1~200)
    ####active_rush+rushId-(1~10)
    for x in range(1, key_max):
        container.append("active_rush_key_set"+str(x))
        active_rush_specific = "active_rush"+str(x)
        container.append(active_rush_specific)
        for j in range(1, 11):
            container.append(active_rush_specific+"-"+str(j))

    print ' '.join(container)

    keyfile= file("redis_key_generation.txt", "w+")
    keyfile.writelines(' '.join(container))
    keyfile.close()

