#! /usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'shichaoyang'

import sys
from rediscluster import StrictRedisCluster

#=========DataReading=========
#get redis operation instance
def generateRedisInstance(redisConnection):
    return None

#get data from active_rush_key_set+rushId zset
def readActiveRushValues(redisConnection):
    print "====reading active_rush_key_set+rushId,active_rush+rushId(zset,rushId: 0~100)====="
    for i in range(1, 100):
        setKey = "active_rush_key_set"+str(i)
        setMap = redisConnection.zrange(setKey,0,1000,withscores=True)
        if setMap:
            for kk, vv in setMap:
                setVal = toCluster.get(kk)
                print kk, setVal

#get data from rush_info_map map
def readRushInfoMap(redisConnection):
    print "=========reading rush_info_map(hash)============="
    return redisConnection.hgetall("rush_info_map")

#get data from user_rush+rushId map
def readUserRushMap(redisConnection):
    return None

#=========DataSyncing=========
#sync data to active_rush_key_set+rushId zset
def syncActiveRushValues(redisConnection):
    return None

#sync data to rush_info_map map
def readRushInfoMap(redisConnection):
    return None

#sync data to user_rush+rushId map
def readUserRushMap(redisConnection):
    return None

#main entrance
if __name__ == '__main__':
    to_cluster_nodes = [{"host": "10.120.35.7", "port": "12379"}]
    toCluster = StrictRedisCluster(startup_nodes=to_cluster_nodes, decode_responses=True)

    print "=========rush_info_map(hash)============="
    rushInfoMap = toCluster.hgetall("rush_info_map")
    print rushInfoMap
    print type(rushInfoMap)
    # http://www.cnblogs.com/yangyongzhi/archive/2012/09/17/2688326.html dict reference
    #iterator the dict type
    for k, v in rushInfoMap.items():
        print k, v

    print "==============user_rush+rushId(hash,rushId: 0~100)=================="
    for i in range(1, 100):
        redisKey = "user_rush"+str(i)
        userRushMap = toCluster.hgetall(redisKey)
        #if dict is not empty
        #if userRushMap:
        #    for ku,vu in userRushMap:
        #        print ku

    print "====active_rush_key_set+rushId,active_rush+rushId(zset,rushId: 0~100)====="
    for i in range(1, 100):
        setKey = "active_rush_key_set"+str(i)
        setMap = toCluster.zrange(setKey,0,1000,withscores=True)
        print type(setMap)
        if setMap:
            #print setMap
            for kk, vv in setMap:
                setVal = toCluster.get(kk)
                print kk, setVal

