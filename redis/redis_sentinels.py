
__author__ = 'shichaoyang'

#pls refer this article: https://github.com/andymccurdy/redis-py
import redis

from redis.sentinel import Sentinel

def master_slave_mode():
     #################Master-Slave mode####################
    redisConnection = redis.Redis(host="10.154.80.199", port=6379, db=7, password="letvsc@2015")
    print("===============string===================")
    redisConnection.setnx("mytestscy", "hahaha")
    print(redisConnection.get("mytestscy"))
    print("================hash==================")
    redisConnection.hset("mytestscyhash", "name1", "scy")
    print(redisConnection.hgetall("mytestscyhash"))
    print(redisConnection.hget("mytestscyhash", "name1"))
    print("================zset==================")
    redisConnection.zadd("mytestscyzset1","set1","0")
    redisConnection.zadd("mytestscyzset1","set5","3")
    redisConnection.zadd("mytestscyzset1","set6","20")
    redisConnection.zadd("mytestscyzset2","set2","1")
    redisConnection.zadd("mytestscyzset3","set3","10")
    redisConnection.zadd("mytestscyzset4","set4","6")
    redisConnection.zadd("mytestscyzset4","set7","100")

    redisConnection.setnx("set1",100)
    redisConnection.setnx("set2",1)
    redisConnection.setnx("set3",34)
    redisConnection.setnx("set4",56)
    redisConnection.setnx("set5",78)
    redisConnection.setnx("set6",23)
    redisConnection.setnx("set7",41)

    result = redisConnection.zrange("mytestscyzset4",0,100,desc=False, withscores=True)
    print(result)
    for item in result:
        print redisConnection.get(item[0])

    print("================sset==================")
    redisConnection.sadd("mytestscysset1","value")
    redisConnection.sadd("mytestscysset1","value1")
    redisConnection.sadd("mytestscysset1","value2")
    redisConnection.sadd("mytestscysset1","value3")
    redisConnection.sadd("mytestscysset1","value3")
    print redisConnection.spop("mytestscysset1")

    print("================list==================")
    redisConnection.lpush("mytestscylist","l1")
    redisConnection.lpush("mytestscylist","l2")
    redisConnection.lpush("mytestscylist","l3")
    redisConnection.rpush("mytestscylist","l4")
    redisConnection.rpush("mytestscylist","l5")
    print redisConnection.lpop("mytestscylist")

    print("================pub/sub==================")
    #====sever side====
    pubsubInstance = redisConnection.pubsub()
    pubsubInstance.subscribe(["scychannel1","scychannel2"])
    for item in pubsubInstance.listen():
        print item
        if item["type"] == "message":
            print item["data"]

    #====client side code in redis_sentinels_client.py file====

def sentinel_mode():
    master_name = "shoppingcar1"
    master_node = [('usredis1.n.lemall.com', 26379),('usredis2.n.lemall.com', 26379),('usredis3.n.lemall.com', 26379),('usredis4.n.lemall.com', 26379)]
    sentinel = Sentinel(master_node, socket_timeout=10, password="letvsc@2015",db=3)
    master = sentinel.master_for(master_name, socket_timeout=10)
    master.set("foo", "bar")
    #slave = sentinel.slave_for(master_name, socket_timeout=5)
    #result = slave.get('foo')
    print sentinel.discover_master("shoppingcar1")
    print sentinel.discover_slaves("shoppingcar1")

    print sentinel.discover_master("shoppingcar2")
    print sentinel.discover_slaves("shoppingcar2")

    print sentinel.discover_master("shoppingcar3")
    print sentinel.discover_slaves("shoppingcar3")

    print sentinel.discover_master("shoppingcar4")
    print sentinel.discover_slaves("shoppingcar4")

    print sentinel.sentinels

if __name__ == "__main__":
    #master_slave_mode()
    sentinel_mode()



