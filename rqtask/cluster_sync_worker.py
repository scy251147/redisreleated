#! /usr/bin/env python
# -*- coding:gbk -*-

import time
import redis
from rediscluster import StrictRedisCluster

__author__ = 'shichaoyang'

#the max range of the rush_id
key_range_max = 200
time_fmt='%Y-%m-%d %X'

#get redis operation instance
def generate_redis_instance(cluster_nodes):
    try:
        return StrictRedisCluster(startup_nodes=cluster_nodes, decode_responses=True)
    except:
        raise

#get data from active_rush_key_set+rushId zset
#reading active_rush_key_set+rushId,active_rush+rushId(zset,rushId: 0~100)
def read_active_rush_values(redis_from,redis_to):
    print "----------------------------------------"
    print "|start sync active_rush_key_set(zset)"
    keys = []
    count = 0
    for i in range(1, key_range_max):
        set_key = "active_rush_key_set"+str(i)
        set_map = redis_from.zrange(set_key, 0, 999999, withscores=True)
        if set_map:
            for kk, vv in set_map:
                redis_to.zadd(set_key, kk, vv)
                keys.append(kk)
                count = count + 1
    for item in keys:
        result = redis_from.get(item)
        redis_to.setnx(item, result)
        count = count + 1
    print "|totally "+str(count)+" records updated"
    print "|complete sync active_rush_key_set"
    print "----------------------------------------"

#get data from rush_info_map map
def read_rush_info_map(redis_from,redis_to):
    print "----------------------------------------"
    print "|start sync rush_info_map(hash)"
    rush_info_map_values = redis_from.hgetall("rush_info_map")
    count = 0
    for rk, rv in rush_info_map_values.iteritems():
        redis_to.hsetnx("rush_info_map", rk, rv)
        count = count + 1
    print "|totally "+str(count)+" records updated"
    print "|complete sync rush_info_map"
    print "----------------------------------------"

#get data from user_rush+rushId map
def read_user_rush_map(redis_from,redis_to):
    print "----------------------------------------"
    print "|start sync user_rush(hash)"
    count = 0
    for i in range(1, key_range_max):
        redis_key = "user_rush"+str(i)
        user_rush_map = redis_from.hgetall(redis_key)
        #if dict is not empty
        if user_rush_map:
            for ku, vu in user_rush_map.iteritems():
                #filter out dirty data
                if not (str(ku).startswith("90xx") or str(ku).startswith("9999")):
                    redis_to.hsetnx(redis_key, ku, vu)
                    count = count + 1
    print "|totally "+str(count)+" records updated"
    print "|complete sync user_rush"
    print "----------------------------------------"

def work_go():
    ##live redis cluster
    #redis_from_nodes = [{"host": "10.212.23.190", "port": "11379"}]
    #redis_from = generate_redis_instance(cluster_nodes=redis_from_nodes)
    ##backup redis cluster
    #redis_to_nodes = [{"host": "10.120.35.0", "port": "12379"}]
    #redis_to = generate_redis_instance(cluster_nodes=redis_to_nodes)

    try:
        #test
        redis_from_nodes = [{"host": "10.120.35.0", "port": "12379"}]
        redis_from = generate_redis_instance(cluster_nodes=redis_from_nodes)
        redis_to = redis.Redis(host="10.120.16.45", port=6379, db=3, password="letvsc@2015")

        print "start:"+time.strftime(time_fmt, time.localtime())
        read_rush_info_map(redis_from, redis_to)
        #read_active_rush_values(redis_from, redis_to)
        #read_user_rush_map(redis_from, redis_to)
        print "end:"+time.strftime(time_fmt, time.localtime())
    except:
        return "failed to run!"
        raise
    else:
        return "success to run!"
