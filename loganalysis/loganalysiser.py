#! /usr/bin/env python
#coding=utf-8
from __future__ import unicode_literals

import sys
import re
reload(sys)
sys.setdefaultencoding('utf-8')

__author__ = 'shichaoyang'

def regex_match(log_file):
    dict = {}
    for line in log_file:
        match = re.findall('query/(.*?).json', line, re.M|re.I)
        for result in match:
            dict[result] = result
    return dict

def copy_to(dict_from,dict_to):
    for k,v in dict_from.iteritems():
        dict_to[k] = v
    return dict_to

if __name__ == "__main__":

    dict_all = {}
    log_file0 = open("log/address_lemall_accept.log");copy_to(regex_match(log_file0), dict_all)
    log_file2 = open("log/address_lemall_accept.log.20161221");copy_to(regex_match(log_file2), dict_all)
    log_file3 = open("log/address_lemall_accept.log.20161222");copy_to(regex_match(log_file3), dict_all)
    log_file4 = open("log/address_lemall_accept.log.20161223");copy_to(regex_match(log_file4), dict_all)
    log_file5 = open("log/address_lemall_accept.log.20161224");copy_to(regex_match(log_file5), dict_all)
    log_file6 = open("log/address_lemall_accept.log.20161225");copy_to(regex_match(log_file6), dict_all)
    log_file7 = open("log/address_lemall_accept.log.20161114");copy_to(regex_match(log_file7), dict_all)
    log_file8 = open("log/address_lemall_accept.log.20161201");copy_to(regex_match(log_file8), dict_all)
    log_file9 = open("log/address_lemall_accept.log.20161212");copy_to(regex_match(log_file9), dict_all)
    log_file10 = open("log/address_lemall_accept.log.20161216");copy_to(regex_match(log_file10), dict_all)

    for k, v in dict_all.iteritems():
        print '/api/web/query/'+k+'.json'