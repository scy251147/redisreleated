#! /usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'shichaoyang'

import pika,sys

if __name__ == '__main__':
    credentials = pika.PlainCredentials("guest","guest")
    conn_params = pika.ConnectionParameters("192.168.59.100",credentials = credentials);
    conn_broker = pika.BlockingConnection(conn_params)
    channel = conn_broker.channel()
    channel.exchange_declare(exchange="hello-rabbitmq",
                             exchange_type="direct",
                             passive=False,
                             durable=True,
                             auto_delete=False
                            )
    msg = sys.argv[1]
    msg_props = pika.BasicProperties()
    msg_props.content_type = "text/plain"

    channel.basic_publish(body=msg,
                          exchange="hello-rabbitmq",
                          properties=msg_props,
                          routing_key="hola"
                         )