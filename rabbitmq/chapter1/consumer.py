#! /usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'shichaoyang'

import pika, sys

if __name__ == '__main__':
    credentials = pika.PlainCredentials("guest","guest")
    conn_params = pika.ConnectionParameters("192.168.59.100",credentials=credentials)
    conn_broker = pika.BlockingConnection(conn_params)
    channel = conn_broker.channel()
    channel.exchange_declare(exchange="hello-rabbitmq",
                             exchange_type="direct",
                             passive=False,
                             durable=True,
                             auto_delete=False
                            )
    channel.queue_declare(queue="hello-queue")
    channel.queue_bind(queue="hello-queue",exchange="hello-rabbitmq",routing_key="hola")

    def msg_consumer(channel,method,header,body):
        channel.basic_ack(delivery_tag=method.delivery_tag)
        if body=="quit":
            channel.basic_cancel(consumer_tag="hello-consumer")
            channel.stop_consuming()
        else:
            print body

        return

    channel.basic_consume(msg_consumer,queue="hello-queue",consumer_tag="hello-consumer")
    channel.start_consuming()