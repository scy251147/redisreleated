#! /usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'shichaoyang'

import sys
import pika

if __name__ == '__main__':
    connection = pika.BlockingConnection(pika.ConnectionParameters('192.168.59.100'))
    channel = connection.channel()
    channel.queue_declare(queue='hello')
    if len(sys.argv)<2:
        print 'message empty!'
        sys.exit(0)
    message = sys.argv[1]
    channel.basic_publish(exchange = '', routing_key='hello', body = message)
    print "[x] sent: '" + message + "'\n"
    connection.close()